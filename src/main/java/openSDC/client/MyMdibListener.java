package openSDC.client;

import java.util.List;

import org.ws4d.java.client.SearchParameter;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.proxy.callbacks.MdibChangedListener;
import com.draeger.medical.biceps.client.proxy.callbacks.MdibMDSListener;
import com.draeger.medical.biceps.client.proxy.control.BICEPSClientTransmissionInformationContainer;
import com.draeger.medical.biceps.client.proxy.control.BICEPSNumericMetricControl;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMetric;
import com.draeger.medical.biceps.client.proxy.impl.metric.DefaultBICEPSMetricState;
import com.draeger.medical.biceps.client.proxy.state.BICEPSNumericMetricState;
import com.draeger.medical.biceps.client.proxy.utils.ProxyUniqueID;
import com.draeger.medical.biceps.common.model.AbstractMetricState;
import com.draeger.medical.biceps.common.model.CodedValue;
import com.draeger.medical.biceps.common.model.NumericMetricDescriptor;
import com.draeger.medical.biceps.common.model.NumericMetricState;
import com.draeger.medical.biceps.common.model.StringMetricState;


public class MyMdibListener implements MdibMDSListener, MdibChangedListener
{
	private ClientListener clientListener = null;
	
	private MyMultiListener mListener = new MyMultiListener();

	
	public void setClientListener( ClientListener listener )
	{
		clientListener = listener;
	}
	
	
	public void noDevicesFound(SearchParameter search) 
	{
		Log.info("No Devices found!");
		
		if( clientListener != null )
		{
			clientListener.noDevicesFound();
		}
	}

	
	public synchronized void newDeviceAvailable(List<? extends BICEPSMedicalDeviceSystem> deviceMdsList)
	{		
		if (deviceMdsList!=null && deviceMdsList.size()>0)
		{
			for (BICEPSMedicalDeviceSystem clientMedicalDeviceSystem : deviceMdsList)
			{
				// TODO: check for null
				String manufacturerName = "NO_METADATA_manufacturerName";
				String modelName = "NO_METADATA_modelName";
				String proxyUniqueID = "NO_METADATA_proxyUniqueID";
				String epr = "";
				if(clientMedicalDeviceSystem.getDescriptor().getMetaData() != null) {
					manufacturerName = clientMedicalDeviceSystem.getDescriptor().getMetaData().getManufacturer().get(0).getValue();
					modelName = clientMedicalDeviceSystem.getDescriptor().getMetaData().getModelName().get(0).getValue();
					proxyUniqueID = clientMedicalDeviceSystem.getProxyUniqueID().getStringRepresentation();
					epr = clientMedicalDeviceSystem.getEndpointReference().getAddress().toString();
				}
				
	
				Log.info("### New device available:");
				Log.info("+ " + manufacturerName);
				Log.info("+ " + modelName);
				Log.info("+ " + proxyUniqueID);
				Log.info("+ " + epr);
				
				if( clientListener != null )
				{					
					clientListener.newDeviceAvailable(clientMedicalDeviceSystem, manufacturerName, modelName, proxyUniqueID);
				}
				
				//check whether we like to do something with this device
				handleDevicesOfInterest(clientMedicalDeviceSystem);

				
//				handleOSCLibTestDevice(clientMedicalDeviceSystem);
				
			}
		}
	}

	
	public void deviceRemoved(List<? extends BICEPSMedicalDeviceSystem> deviceMdsList) 
	{		
		if( deviceMdsList != null && deviceMdsList.size() > 0 )
		{
			for (BICEPSMedicalDeviceSystem clientMedicalDeviceSystem : deviceMdsList)
			{				
				String manufacturerName = clientMedicalDeviceSystem.getDescriptor().getMetaData().getManufacturer().get(0).getValue();
				String modelName = clientMedicalDeviceSystem.getDescriptor().getMetaData().getModelName().get(0).getValue();
				String proxyUniqueID = clientMedicalDeviceSystem.getProxyUniqueID().getStringRepresentation();
	
				Log.info("Device removed:");
				Log.info("+ " + manufacturerName);
				Log.info("+ " + modelName);
				Log.info("+ " + proxyUniqueID);
				
				if( clientListener != null )
				{
					clientListener.deviceRemoved(proxyUniqueID);
				}
			}
		}
	}
	
	
	/**
	 * this method will check whether we like to something with the device we found
	 * @param clientMedicalDeviceSystem
	 */
	private void handleDevicesOfInterest(BICEPSMedicalDeviceSystem clientMedicalDeviceSystem) {
	

		String epr = clientMedicalDeviceSystem.getEndpointReference().getAddress().toString();
		
		if(epr.equals(Constants.getInstance().getActualErp())){
			
			System.out.println("[[[OK]]] TestDevice discovery successful");
			//get all metrics
			List<BICEPSMetric> metrics = DeviceClient.getInstance().getBicepsClient().getMetrics(clientMedicalDeviceSystem);
			
			System.out.println("[INFO] Display all metrics (and subscribe if possible");
			//walk through these metrics
			for (BICEPSMetric bicepsMetric : metrics) {
				
				System.out.println("[[[OK]]] Metric found with descriptor handle: " + bicepsMetric.getDescriptor().getHandle());
				
				AbstractMetricState ams = bicepsMetric.getStateProxy().getState();
				if(ams instanceof NumericMetricState){
					System.out.println("   [[[OK]]] current value: " + ((NumericMetricState)ams).getObservedValue().getValue());
				}
				else if(ams instanceof StringMetricState){
					System.out.println("   [[[OK]]] current value: " + ((StringMetricState)ams).getObservedValue().getValue());
				} 



				//subscribe to this metric
				bicepsMetric.subscribe(mListener);
				

				
				if(bicepsMetric instanceof BICEPSNumericMetricState 
						&& bicepsMetric.getDescriptor().getHandle().equals(Constants.HANDLE_METRIC)){
					
					System.err.println("--- Metric found ---");
					
					BICEPSNumericMetricControl controlProxy = ((BICEPSNumericMetricState) bicepsMetric).getControlProxy();
					
					float oldValue = ((NumericMetricState)ams).getObservedValue().getValue().floatValue();
					
					if(controlProxy != null){
//						for(int i = 0; i < 10; i++){
//							try {
//								Thread.sleep(500);
//								System.err.print(".");
//							} catch (InterruptedException e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//						}
//						System.err.println();
//						
//						System.out.println(System.currentTimeMillis() + "[Info] try to change this metric (This SHOULD lead to an event.)");
//						controlProxy.setNumeric(oldValue + (float) 42, (BICEPSClientTransmissionInformationContainer) null);
//						System.out.println(System.currentTimeMillis() + "[Info] try to change this metric DONE.");

						
						//create the thread that will trigger the requests for the measuremants
						Thread thread = new Thread(new RequestThread(bicepsMetric));
						thread.start();
					}
				}
			}
			


			

		}
		else{
			System.err.println("---- ERP (" + epr +  "!=" + Constants.getInstance().getActualErp()+")****");
		}
	}
	
	
	
	public void newMetricAvailable(ProxyUniqueID proxyUniqueID, CodedValue code)
	{
		Log.info("New Metric Available: "+proxyUniqueID + " Code: " + code);
	}

	
	public void newManeuverAvailable(ProxyUniqueID proxyUniqueID, CodedValue code)
	{
		Log.info("New Maneuver Available: "+proxyUniqueID + " Code: " + code);	
	}

	
	public void newStreamAvailable(ProxyUniqueID proxyUniqueID, CodedValue code)
	{
		Log.info("New Stream Available: " + proxyUniqueID + " Code: " + code);
	}

	
	public void newAlertAvailable(ProxyUniqueID proxyUniqueID,CodedValue code)
	{
		Log.info("New Alert Available: " + proxyUniqueID + " Code: " + code);	
	}

	
	public void newAlertSystemAvailable(ProxyUniqueID proxyUniqueID)
	{
		Log.info("New Alert System Available: " + proxyUniqueID);
	}

	
	public void newAlertSignalAvailable(ProxyUniqueID proxyUniqueID)
	{
		Log.info("New Alert Signal Available: " + proxyUniqueID);	
	}


		


}
