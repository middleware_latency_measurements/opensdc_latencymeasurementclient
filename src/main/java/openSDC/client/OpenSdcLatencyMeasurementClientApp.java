package openSDC.client;

//import javax.realtime.RealtimeThread;
import org.ws4d.java.util.Log;

import uro.latencyMeasurement.LatencyMeasurement;




public class OpenSdcLatencyMeasurementClientApp
{
	public static void main(String[] args)
	{

		if(args.length != 4){
			System.err.println("usage: java -D [...] -jar LM_Client #numberOfMeasurements #delayBetweenMeasurements #delayBeforeMeasurements <EPR Postfix>");
			System.exit(0);
		}
		
		int numMeasurements = Integer.parseInt(args[0]);
		int delayBetweenMeasurements = Integer.parseInt(args[1]);
		int delayBeforeMeasurements = Integer.parseInt(args[2]);
		
		LatencyMeasurement.getInstance().setNumMeasurements(numMeasurements);
		LatencyMeasurement.getInstance().setDelayBetweenMeasurements(delayBetweenMeasurements);
		LatencyMeasurement.getInstance().setDelayBeforeMeasurements(delayBeforeMeasurements);
		
		String tmp = Constants.getInstance().getActualErp();
		Constants.getInstance().setActualErp(tmp+args[3]);
		
		System.err.println("useJamaicaVM: " + LatencyMeasurement.getInstance().useJamaicaVM);
		
		if( LatencyMeasurement.getInstance().useJamaicaVM ) {
//			RealtimeThread t = new RealtimeThread(){
//				
//				@Override
//				public void run() {
//					try
//					{			
//	
//						
//	//					##################################
//						
//						Log.setLogLevel(Log.DEBUG_LEVEL_INFO);
//	
//						// create device client
//						DeviceClient client = DeviceClient.getInstance(); //TODO kcConnector
//	
//						//start searching devices
//						client.searchMedicalDevices();
//						
//						while(true){
//							Thread.sleep(100);
//						}
//	
//					}
//					catch(Exception e)
//					{
//						e.printStackTrace();
//					}
//				}
//			};
//				
//			t.start();
		} else {
		
			try
			{			
	
				
	//			##################################
				
				Log.setLogLevel(Log.DEBUG_LEVEL_INFO);
	
				// create device client
				DeviceClient client = DeviceClient.getInstance(); //TODO kcConnector
	
				//start searching devices
				client.searchMedicalDevices();
			
	//			//some interaction
	//			String input = ""; //will contain the console input
	//			Scanner scanner = new Scanner(System.in); //we will use it to read from the console
	//			
	//			while(!input.equals("x")) { //"x" as about input
	//				System.out.println("Insert command. (end program with \"x\"; search with \"s\")");
	//				
	//				//read console input
	//				input = scanner.nextLine();
	//				
	//				//abort criterion
	//				if (input.equals("x")) {
	//					break; //leave the loop
	//				}
	//				
	//				//search
	//				if (input.equals("s")) {
	//					client.searchMedicalDevices();
	//				}
	//			}
	//			
	//			scanner.close();
				
				while(true){
					Thread.sleep(100);
				}
	
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}
