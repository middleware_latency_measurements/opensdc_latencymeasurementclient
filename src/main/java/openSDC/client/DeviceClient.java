package openSDC.client;

import org.ws4d.java.types.URI;
import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.BicepsClientFramework;
import com.draeger.medical.biceps.client.IBICEPSClient;

public class DeviceClient
{
	private static DeviceClient instance;
	
	private IBICEPSClient diceClient;
	private MyMdibListener mdibListener;
	private MyMainProcess mainThread = new MyMainProcess(this);
	
	
	public static DeviceClient getInstance()
	{
		if( instance == null )
		{
			instance = new DeviceClient();
		}
		
		return instance;
	}
	
	private DeviceClient()
	{
		try
		{
			String dpwsPropertiesFile = System.getProperty("MDPWS.PropertiesFile", "config/mdpws.properties");
			
			//setup the dice client framework
			BicepsClientFramework.getInstance().setup(null, new URI(dpwsPropertiesFile));
	
			//get an instance of a client
			this.diceClient = BicepsClientFramework.getInstance().getClient();
	
			//we want to listen to MDIB events (new device...)
			mdibListener = new MyMdibListener();
			
			diceClient.subscribe(mdibListener);
			
	
			//enable Plug'n' Play
			diceClient.setPnpEnabled(true);
	
			//So we need some kind of application thread
			Thread t=new Thread(mainThread);
			t.start();
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}
	}

	public IBICEPSClient getBicepsClient()
	{
		return diceClient;
	}
	
	public void setClientListener( ClientListener listener )
	{
		if(mdibListener != null)
		{
			mdibListener.setClientListener(listener);
		}
	}

	public void searchMedicalDevices()
	{
		Log.info("Start new device search");
		diceClient.searchDevices(null);
	}

	static class MyMainProcess implements Runnable
	{
		boolean isRunning=true;
		DeviceClient  ddc=null;

		public MyMainProcess(DeviceClient ddc)
		{
			super();
			this.ddc = ddc;
		}

		
		public void run()
		{
			System.out.println("Started");
//			this.ddc.searchMedicalDevices();
			while(isRunning)
			{
				try
				{
					Thread.sleep(500);
				}
				catch (InterruptedException e)
				{
					Log.printStackTrace(e);
				}
			}
		}

	}
}
