package openSDC.client;

import java.math.BigDecimal;
import java.util.List;

import org.ws4d.java.util.Log;

import com.draeger.medical.biceps.client.callbacks.SubscriptionEndCodeType;
import com.draeger.medical.biceps.client.proxy.callbacks.AlertConditionListener;
import com.draeger.medical.biceps.client.proxy.callbacks.AlertSignalListener;
import com.draeger.medical.biceps.client.proxy.callbacks.ChangedProperty;
import com.draeger.medical.biceps.client.proxy.callbacks.ManeuverListener;
import com.draeger.medical.biceps.client.proxy.callbacks.MetricListener;
import com.draeger.medical.biceps.client.proxy.callbacks.StreamListener;
import com.draeger.medical.biceps.client.proxy.control.BICEPSAlertConditionControl;
import com.draeger.medical.biceps.client.proxy.control.BICEPSAlertSignalControl;
import com.draeger.medical.biceps.client.proxy.control.BICEPSManeuver;
import com.draeger.medical.biceps.client.proxy.control.BICEPSMetricControl;
import com.draeger.medical.biceps.client.proxy.control.BICEPSNumericMetricControl;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertCondition;
import com.draeger.medical.biceps.client.proxy.description.BICEPSAlertSignal;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMetric;
import com.draeger.medical.biceps.client.proxy.description.BICEPSStream;
import com.draeger.medical.biceps.client.proxy.state.BICEPSNumericMetricState;
import com.draeger.medical.biceps.client.proxy.state.BICEPSStreamState;
import com.draeger.medical.biceps.client.proxy.state.BICEPSStringMetricState;
import com.draeger.medical.biceps.common.model.InvocationError;
import com.draeger.medical.biceps.common.model.InvocationState;
import com.draeger.medical.biceps.common.model.NumericMetricState;
import com.draeger.medical.biceps.common.model.PausableActivation;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayMetricState;
import com.draeger.medical.biceps.common.model.RealTimeSampleArrayValue;
import com.draeger.medical.biceps.common.model.StringMetricState;

public class MyMultiListener implements 
MetricListener, StreamListener, AlertSignalListener, AlertConditionListener, ManeuverListener
{
	public boolean useOSCBCommunication = false;
	public boolean useSocketCommunication = true;

	
	public static final String SOCKET_SERVER_IP = "localhost";
	public static final int SOCKET_SERVER_PORT = 4242;
	
	public void changedMetric(BICEPSMetric source, List<? extends ChangedProperty> changedProps)
	{		
		
		
		
		if (source.isStateValid())
		{		
			if (source instanceof BICEPSNumericMetricState)
			{

				NumericMetricState nms = ((BICEPSNumericMetricState)source).getState();
				if(nms != null && nms.getObservedValue() != null)
				{
//					System.err.println(System.nanoTime() +" <IN - APPLICATION>" + " [[[EVENT]]] new numeric value of " + source.getDescriptor().getHandle() + " is: " + nms.getObservedValue().getValue());						
				}

			}
			else if (source instanceof BICEPSStringMetricState)
			{
				StringMetricState sms = ((BICEPSStringMetricState)source).getState();
				if(sms != null && sms.getObservedValue() != null)
				{
//					System.err.println(System.nanoTime() +" <IN - APPLICATION>" + " [[[Event]]] new string value of " + source.getDescriptor().getHandle() + " is: " + sms.getObservedValue().getValue());
				}
			}
			else{
				System.err.println("[[[ERROR]]] Received metric event of an unhandled type!");
			}
		}

	}
	
	
	
	public void changeRequestStateChanged(BICEPSMetricControl source, long transactionId, InvocationState newState, InvocationError errorCode, String errorMsg)
	{
		
		if (source instanceof BICEPSNumericMetricControl)
		{
//			System.out.println(System.nanoTime() +" <IN - APPLICATION>" + "[[[Event]]] changeRequestStateChanged (Numeric): "+transactionId+" "+newState);	
		}
		else if (source instanceof BICEPSStringMetricState)
		{
//			System.out.println(System.nanoTime() +" <IN - APPLICATION>" + "[[[Event]]] changeRequestStateChanged (String): "+transactionId+" "+newState);
		}

	}

	
	public void removedMetric(BICEPSMetric source) 
	{
		Log.info("Removed Metric: " +source.getProxyUniqueID() );
	}

	
	public void subscriptionEnded(BICEPSMetric source, SubscriptionEndCodeType reason) 
	{
		Log.info("Subscription Ended");
	}

	
	public void receivedStreamData(BICEPSStream source, RealTimeSampleArrayValue realTimeSampleArrayValue) 
	{
		Log.info("Changed Stream: "+source.getProxyUniqueID());
		if (source instanceof BICEPSStreamState)
		{
			List<RealTimeSampleArrayMetricState> bufferContent = ((BICEPSStreamState)source).getBuffer();
			for (RealTimeSampleArrayMetricState realTimeSampleArrayMetricState : bufferContent) {
				RealTimeSampleArrayValue obsVal = (RealTimeSampleArrayValue )realTimeSampleArrayMetricState.getObservedValue();
				List<BigDecimal> values = obsVal.getValues();
				System.out.println(values.toString());
			}

		}
	}

	
	public void removedStream(BICEPSStream source)
	{
		Log.info("Removed Stream: " + source.getProxyUniqueID() );
	}




	public void changedAlertSignal(BICEPSAlertSignal source,
			List<ChangedProperty> changedProps) {
		System.err.println("changedAlertSignal NOT implemented");
	}
	



	public void invokationStateChanged(BICEPSAlertSignalControl source,
			long transactionId, InvocationState newState,
			InvocationError errorCode, String errorMsg) {
		// TODO Auto-generated method stub
		
	}


	public void removedAlertSignal(BICEPSAlertSignal source) {
		// TODO Auto-generated method stub
		
	}


	public void subscriptionEnded(BICEPSAlertSignal source,
			SubscriptionEndCodeType reason) {
		// TODO Auto-generated method stub
		
	}

	public void changedAlertCondition(BICEPSAlertCondition source,
			List<ChangedProperty> changedProps) {
		
		if (source.isStateValid())
		{
			if (source instanceof BICEPSAlertCondition)
			{
				PausableActivation lpa = ((BICEPSAlertCondition)source).getStateProxy().getState().getState();
				boolean presence = ((BICEPSAlertCondition)source).getStateProxy().getState().isPresence();				
				System.out.println("[[[Event]]] New alert condition state: " + source.getDescriptor().getHandle() + 
						": " + lpa + " --- Presence: " + presence);
			}
		}
		
	}

	public void invokationStateChanged(BICEPSAlertConditionControl source,
			int transactionId, InvocationState newState,
			InvocationError errorCode, String errorMsg) {
		// TODO Auto-generated method stub
		
	}

	public void removedAlertCondition(BICEPSAlertCondition source) {
		// TODO Auto-generated method stub
		
	}

	public void subscriptionEnded(BICEPSAlertCondition source,
			SubscriptionEndCodeType reason) {
		// TODO Auto-generated method stub
		
	}



	public void changedManeuver(BICEPSManeuver source,
			List<ChangedProperty> changedProps) {
		System.out.println( "[[[Event]]] changedManeuver received" );
		
	}



	public void requestStateChanged(BICEPSManeuver source, long transactionId,
			InvocationState newState, InvocationError errorCode, String errorMsg) {
		System.out.println( "[[[Event]]] changeRequestStateChanged received" );
		System.out.println("   [[[Event]]] changeRequestStateChanged (Maneuver): "+transactionId+" "+newState);	
		
	}



	public void removedManeuver(BICEPSManeuver source) {
		System.out.println( "[[[Event]]] removedManeuver received" );
		
	}



	public void subscriptionEnded(BICEPSManeuver source,
			SubscriptionEndCodeType reason) {
		System.out.println( "[[[Event]]] subscriptionEnded received" );
	}

}
