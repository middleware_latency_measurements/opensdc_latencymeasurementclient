package openSDC.client;

public class Constants {
	
	
	private static final String EPR = "EPR_OpenSDC_LATENCY_MEASUREMENT_";
	public static final String HANDLE_METRIC = "handle_metric";
	
	private String actualErp = "";
	
	static Constants shared = null;
	
	private Constants() {
		// TODO Auto-generated constructor stub
	}
	
	public static Constants getInstance(){
		if(shared == null){
			shared = new Constants();
			
			shared.setActualErp(EPR);
		}
		
		return shared;
	}

	public String getActualErp() {
		return actualErp;
	}

	public void setActualErp(String actualErp) {
		this.actualErp = actualErp;
	}

}
