package openSDC.client;

import java.util.Scanner;

import uro.latencyMeasurement.LatencyMeasurement;

import com.draeger.medical.biceps.client.proxy.control.BICEPSNumericMetricControl;
import com.draeger.medical.biceps.client.proxy.description.BICEPSMetric;
import com.draeger.medical.biceps.client.proxy.impl.metric.DefaultBICEPSMetricState;
import com.draeger.medical.biceps.client.proxy.state.BICEPSNumericMetricState;
import com.draeger.medical.biceps.common.model.NumericMetricDescriptor;

//import javax.realtime.Clock;
//import javax.realtime.AbsoluteTime;

public class RequestThread implements Runnable {

	BICEPSMetric metric = null;
	
	public RequestThread(BICEPSMetric metric) {
		super();
		
		this.metric = metric;
		
	}

	public void run() {
		//some interaction
		String input = ""; //will contain the console input
		Scanner scanner = new Scanner(System.in); //we will use it to read from the console
		
		while(!input.equals("s")) { //"x" as about input
			System.out.println("Insert command. (start measurement with \"s\")");
			
			//read console input
			input = scanner.nextLine();
			
			//abort criterion
			if (input.equals("s")) {
				break; //leave the loop
			}
		}
		
		scanner.close();
		
		try {
			Thread.sleep(LatencyMeasurement.getInstance().getDelayBeforeMeasurements());
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		int sleepTime = LatencyMeasurement.getInstance().getDelayBetweenMeasurements(); 
		
		//clear all measurements before starting the real measurements
		LatencyMeasurement.getInstance().clearAllMeasurements();
		
		for(int measurementCount = 0;
				measurementCount < LatencyMeasurement.getInstance().getNumMeasurements(); 
				measurementCount++){


			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (LatencyMeasurement.getInstance().useJamaicaVM) {
//				AbsoluteTime t0 = Clock.getRealtimeClock().getTime();
//				LatencyMeasurement.getInstance().addElementToT0List(t0.getMilliseconds() + "");
//				LatencyMeasurement.getInstance().addElementToT0NanoList(t0.getNanoseconds() + "");
////				System.err.println("t0: " + t0.getMilliseconds() + "," + t0.getNanoseconds());
			} else {
				LatencyMeasurement.getInstance().addElementToT0List(System.nanoTime() + "");
//				System.err.println("t0: " + System.nanoTime());
			}		
			
			DefaultBICEPSMetricState dbms = (DefaultBICEPSMetricState<NumericMetricDescriptor>) metric.getStateProxy();
			dbms.touch();
			
			if (LatencyMeasurement.getInstance().useJamaicaVM) {
//				AbsoluteTime t6 = Clock.getRealtimeClock().getTime();
//				LatencyMeasurement.getInstance().addElementToT6List(t6.getMilliseconds() + "");
//				LatencyMeasurement.getInstance().addElementToT6NanoList(t6.getNanoseconds() + "");
////				System.err.println("t6: " + t6.getMilliseconds() + "," + t6.getNanoseconds());
			} else {
				LatencyMeasurement.getInstance().addElementToT6List(System.nanoTime() + "");
//				System.err.println("t6: " + System.nanoTime());
			}	
			
			BICEPSNumericMetricControl controlProxy = ((BICEPSNumericMetricState) metric).getControlProxy();
//			System.err.println("current value: " +controlProxy.getStateProxy().getState().getObservedValue().getValue());
			

		}
		
		//write data into file
		LatencyMeasurement.getInstance().writeEveryMeasurementToFiles(true);
			
	}

}
