package openSDC.client;

import com.draeger.medical.biceps.client.proxy.description.BICEPSMedicalDeviceSystem;

public interface ClientListener
{
	public void noDevicesFound();
	
	public void newDeviceAvailable(BICEPSMedicalDeviceSystem medicalDevice, String manufacturerName, String modelName, String proxyUniqueID);
	
	public void deviceRemoved(String proxyUniqueID);
}
